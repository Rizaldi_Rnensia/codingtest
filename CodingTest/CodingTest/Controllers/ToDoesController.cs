﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CodingTest.Models;

namespace CodingTest.Controllers
{
    [Route("api/ToDo")]
    [ApiController]
    public class ToDoesController : ControllerBase
    {
        private readonly CodingTestContext _context;

        public ToDoesController(CodingTestContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Map of Oject that will contains status, and data response
        /// </summary>
        Dictionary<string, object> mapResult = new Dictionary<string, object>();

        // GET: api/ToDo
        /// <summary>
        /// Get All Todo's
        /// </summary>
        /// <returns>List of Todo Items</returns>
        [HttpGet]
        public IActionResult GetToDoes()
        {
            try
            {
                var data = _context.ToDoes.ToList();
                mapResult.Add("status", "get data success");
                mapResult.Add("data", data);

                return Ok(mapResult);
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: api/ToDo/5
        /// <summary>
        /// Get Specific Todo
        /// </summary>
        /// <param name="id"></param>
        /// <returns>specific ToDo by Id</returns>
        [HttpGet("{id}")]
        public IActionResult GetToDo([FromRoute] int id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var data = _context.ToDoes.FindAsync(id);

                if (data == null)
                {
                    return NotFound();
                }
                mapResult.Add("status", "get data success");
                mapResult.Add("data", data);
            }
            catch (Exception)
            {
                throw;
            }
            

            return Ok(mapResult);
        }
        /// <summary>
        /// Get Incoming ToDo
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        [HttpGet("GetIncoming")]
        public IActionResult GetIncomingToDo([FromRoute] DateTime date)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var data = _context.ToDoes.Where(x => x.Expiry.Value.Day == date.Day).ToList();

                if (data == null)
                {
                    return NotFound();
                }
                mapResult.Add("status", "get data success");
                mapResult.Add("data", data);
            }
            catch (Exception)
            {
                throw;
            }
            return Ok(mapResult);
        }

        // PUT: api/ToDo/5
        /// <summary>
        /// Update Todo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="body"></param>
        /// <returns>No Content</returns>
        [HttpPut("Update/{id}")]
        public IActionResult PutToDo([FromRoute] int id, [FromBody] ToDo body)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var currentData = _context.ToDoes.Where(x => x.Id == id).FirstOrDefault();
            currentData.Expiry = body.Expiry;
            currentData.Title = body.Title;
            currentData.Description = body.Description;
            currentData.Progress = body.Progress;
            currentData.Status = body.Status;
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ToDoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Set Todo percent complete
        /// </summary>
        /// <param name="id"></param>
        /// <returns>No Content</returns>
        [HttpPut("SetPercentToComplete/{id}")]
        public IActionResult SetTodoPercentComplete([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var currentData = _context.ToDoes.Find(id);
            currentData.Progress = "Complete";
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ToDoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Mark Todo As Done
        /// </summary>
        /// <param name="id"></param>
        /// <returns>No Content</returns>
        [HttpPut("MarkTodoAsDone/{id}")]
        public IActionResult MarkTodoAsDone([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var currentData = _context.ToDoes.Find(id);
            currentData.Status = "Done";
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ToDoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ToDo
        /// <summary>
        /// Create Todo
        /// </summary>
        /// <param name="body"></param>
        /// <returns>created todo from body</returns>
        [HttpPost]
        public IActionResult PostToDo([FromBody] ToDo body)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ToDoes.Add(body);
            _context.SaveChanges();

            return CreatedAtAction("GetToDo", new { id = body.Id }, body);
        }

        // DELETE: api/ToDo/5
        /// <summary>
        /// Delete Todo
        /// </summary>
        /// <param name="id"></param>
        /// <returns>object of todo</returns>
        [HttpDelete("{id}")]
        public IActionResult DeleteToDo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var toDo = _context.ToDoes.Find(id);
            if (toDo == null)
            {
                return NotFound();
            }
            _context.ToDoes.Remove(toDo);
            _context.SaveChanges();

            mapResult.Add("status", "delete data success");
            mapResult.Add("data", toDo);

            return Ok(mapResult);
        }

        private bool ToDoExists(int id)
        {
            return _context.ToDoes.Any(e => e.Id == id);
        }
    }
}
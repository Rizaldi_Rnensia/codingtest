﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CodingTest.Models
{
    public partial class CodingTestContext : DbContext
    {
        public CodingTestContext()
        {
        }

        public CodingTestContext(DbContextOptions<CodingTestContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ToDo> ToDoes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Name=CodingTest");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ToDo>(entity =>
            {
                entity.ToTable("ToDo");

                entity.Property(e => e.Expiry).HasColumnType("datetime");

                entity.Property(e => e.Progress).HasMaxLength(50);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.Title).HasMaxLength(50);
            });
        }
    }
}

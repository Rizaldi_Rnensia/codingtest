﻿using System;
using System.Collections.Generic;

namespace CodingTest.Models
{
    /// <summary>
    /// this class represents the ToDo
    /// </summary>
    public partial class ToDo
    {
        /// <summary>
        /// represent unique identifier
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// represent the date of Todo will be exipred
        /// </summary>
        public DateTime? Expiry { get; set; }
        /// <summary>
        /// represent the header title of ToDo
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// represent the content of Todo
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// store the progress of Todo (0%-100% / Completed)
        /// </summary>
        public string Progress { get; set; }
        /// <summary>
        /// indicates the status of Todo (Done)
        /// </summary>
        public string Status { get; set; }
    }
}
